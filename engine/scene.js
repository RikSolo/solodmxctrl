var sDMXscene = angular.module('sDMXscene', ['sDMXchannels', 'sDMXpatch', 'sDMXuniverse']);

sDMXscene.service('sceneService', function(channelService, patchService, universeService){
  this.scenes = [
    { name: "test",
      running: false,
      devices: {
        0:{
          intensity: {v: 127, htp:true}
        }
      }
    }
  ];
  
  this.dumpToScene = function(name){
    var devices = {
      
    }
    
    for(var i in patchService.devices){
      var device = patchService.devices[i];
      for(var k in device.channels){
        if(device.channels[k].v !== 0){
          if(!(i in devices)){
            devices[i] = {};
          }
          devices[i][k] = {v: device.channels[k].v, htp: device.channels[k].htp}
        }
      }
    }
    this.scenes.push({name: name, running:false, devices: devices});
  };
  
  this.startScene = function(sceneId){
    var scene = this.scenes[sceneId];
    scene.running = true;
    for(var i in scene.devices){
      for(var k in scene.devices[i]){
        if(scene.devices[i][k].htp){
          channelService.setHTP(i, k, "s" + parseInt(sceneId), parseInt(scene.devices[i][k].v));
        } else {
          channelService.setLTP(i, k, parseInt(scene.devices[i][k].v));
        }
      }
    }
    universeService.pushUniverses();
  }
  
  this.stopScene = function(sceneId){
    var scene = this.scenes[sceneId];
    scene.running = false;
    for(var i in scene.devices){
      for(var k in scene.devices[i]){
        if(scene.devices[i][k].htp){
          channelService.setHTP(i, k, "s" + parseInt(sceneId), 0);
        } else {
          channelService.setLTP(i, k, 0);
        }
      }
    }
    universeService.pushUniverses();
  }
  
  this.toggleScene = function(sceneId){
    var scene = this.scenes[sceneId];
    if(scene.running){
      this.stopScene(sceneId);
    } else {
      this.startScene(sceneId);
    }
  };
  
  this.setScene = function(sceneId, value){
    var scene = this.scenes[sceneId];
    if((value == 0 && scene.running == true)||(isNaN(value))){
      scene.running = false;
      this.stopScene(sceneId);
    } else {
      scene.running = true;
      for(var i in scene.devices){
        for(var k in scene.devices[i]){
          if(scene.devices[i][k].htp){
            channelService.setHTP(i, k, "s" + parseInt(sceneId), parseInt(scene.devices[i][k].v * (value/255)));
          } else {
            channelService.setLTP(i, k, parseInt(scene.devices[i][k].v * (value/255)));
          }
        }
      }
      universeService.pushUniverses();
    }
  }
  
});

// SCENE TEMPLATE
//{
//  name: "scene name",
//  running: false,
//  devices: {
//    deviceid: {
//      capability: {v: int value, htp: boolean}
//    }
//  }
//}