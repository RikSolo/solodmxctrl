var sDMXinput = angular.module('sDMXinput', []);

sDMXinput.service('inputService', function($http){
  
  var inputUniverse = [];
  

  
  this.getUniverse = function(){
    return inputUniverse;
  }
  
  function getInput(universe){
    if(!universe){
      universe = 99999;
    }
    $http.get('http://localhost:9090/get_dmx?u='+ universe.toString()).then(function(res){
      inputUniverse = res.data.dmx;
    }, function(err){
      clearInterval(interval);
    });
  }

  var interval = setInterval(getInput, 22);
  
})