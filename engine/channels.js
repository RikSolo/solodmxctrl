var sDMXchannels = angular.module('sDMXchannels', ['sDMXpatch', 'sDMXuniverse']);

var NanoTimer = require('nanotimer');

sDMXchannels.factory('channelService', function(patchService, universeService, intervalFactory){  
  this.setHTP = function(device, capability, id, value, fade){
    if(!fade){
      patchService.devices[device].channels[capability].in[id] = value; 
      console.log(patchService.devices[device]);
      patchService.updateChannelHTP(device, capability);
      universeService.calculateUniverses();
    } else {
      var startval = patchService.devices[device].channels[capability].in[id];
      if(!startval){
        startval = 0;
      }
      var endval = value;
      var diff = endval - startval;
      
      if(patchService.devices[device].channels[capability].htp && startval < endval){
        startval = 0;
      }
      
      var time = 0;
      var val = startval;
      
      var interval = 50;
      var stepsize = (interval/fade)*diff;
      
      intervalFactory.interval(function(){
        val += stepsize;
        time += interval;
        if(val > 255){
          val = 255;
        }

        patchService.devices[device].channels[capability].in[id] = Math.round(val);  
        patchService.updateChannelHTP(device, capability);
        universeService.calculateUniverses();
        universeService.pushUniverses();
      }, interval, diff/stepsize);
    }
  }      
  
  this.setLTP = function(device, capability, value){
    patchService.devices[device].channels[capability].v = value;
    universeService.calculateUniverses();
  }
  return this;
  
});
sDMXchannels.factory('intervalFactory', function($interval){
  var test = {};
  test.interval = function(cb, delay, steps){
    var interval = $interval(function(){cb();}, delay, steps);
  }
  return test;
})
