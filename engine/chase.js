var sDMXchase = angular.module('sDMXchase', ['sDMXchannels', 'sDMXuniverse']);

sDMXchase.service('chaseService', function(channelService, universeService){
  
  this.setChase = function(chaseId, value){
    var chase = this.chases[chaseId];
    if(value > 0){
      this.startChase(chaseId, chase.step, value);
    } else if(value == 0) {
      this.stopChase(chaseId);
    }
  }
  
  this.nextStep = function(chaseId){
    var chase = this.chases[chaseId];
    var nextStep = parseInt(chase.step + 1);
    if(nextStep > chase.steps.length - 1){
      nextStep = 0;
      chase.step = 0;
    }
    if(chase.running){
      this.stopChase(chaseId, true);
      this.startChase(chaseId, nextStep, chase.val, true);
    }
    universeService.pushUniverses();
  }
  
  this.startChase = function(chaseId, step, value, nopush){
    var chase = this.chases[chaseId];
    if(!step){
      step = chase.step;
    }
    
    if(!value){
      value = 255;
    }
    
    chase.running = true;
    chase.step = step;
    chase.val = value;
    
    for(var i in chase.steps[step].devices){
      var device = chase.steps[step].devices[i];
      for(var k in device){
        if(device[k].htp){
          channelService.setHTP(i, k, "c" + parseInt(chaseId), device[k].v * (chase.val/255));
        } else {
          channelService.setLTP(i, k, device[k].v);
        }
      }
    }
    if(nopush != true){
      universeService.pushUniverses();
    }
  };
  
  this.stopChase = function(chaseId, nopush){
    var chase = this.chases[chaseId];
    chase.running = false;
    for(var i in chase.steps[0].devices){
      var device = chase.steps[0].devices[i];
      for(var k in device){
        if(device[k].htp){
          channelService.setHTP(i, k, "c" + parseInt(chaseId), 0);
        } else {
          channelService.setLTP(i, k, 0);
        }
      }
    }
    if(nopush != true){
      universeService.pushUniverses();
      chase.val = 0;
    }
  };
  
  this.chases = [
    {
      name: "test",
      running:false,
      val: 0,
      step: 0,
      steps: [
        {
          hold: 1,
          devices: {
            0:{
              intensity: {v: 255, htp: true},
              intensity2: {v:127, htp: true}
            }
          }
        },
        {
          hold: 1,
          devices:{
            0:{
              intensity: {v: 127, htp: true},
              intensity2: {v:255, htp:true}
            }
          }
        }
      ]
    }
  ];
});

//CHASER TEMPLATE
//{
//  name: string name,
//  running: boolean running,
//  steps: [
//    {
//      hold: int time in ms,
//      devices{
//        deviceid:{
//          capability: {v: int value, htp: boolean htp}
//          ...
//        } ...
//      }
//    } ...
//  ]
//}
