var sDMXpatch = angular.module('sDMXpatch', []);

var jsonfile = require('jsonfile');


sDMXpatch.service('patchService', function(){
  this.devices = [
    /*{name: "test",
    universe: 0,
    startchannel: 1,
    channels: {
      intensity: {v: 0, in: {m:0}, id:0, htp: true},
      intensity2: {v: 0, in:{m:0}, id:1, htp: true}
    }},
    {name: "rgbtest",
    startchannel: 3,
    universe: 0,
    channels: {
      red: {v: 0, in: {m:0}, id:0, htp: true},
      green: {v: 0, in: {m:0}, id:1, htp: true},
      blue: {v: 0, in: {m:0}, id:2, htp: true}
    }}*/
  ];
  
  this.updateChannelHTP = function(device, capability){
    var values = [];
    for(var i in this.devices[device].channels[capability].in){
      values.push(this.devices[device].channels[capability].in[i]);
    }
    var value = Math.max.apply(Math, values);
    this.devices[device].channels[capability].v = value;
    console.log(value);
    console.log(device);
  }
  
  this.addDevice = function(universe, startchannel, amount){
    var deviceArray = [];
    var file = dialog.showOpenDialog({properties: ['openFile'], filters: [{name: 'sDMX device', extensions: ['sdmxd']}], defaultPath: './'});
    if(file){
     var obj = jsonfile.readFileSync(file[0])
      for(var i = 0; i < amount; i++){
        var schannel = parseInt(startchannel) + parseInt(Object.keys(obj.channels).length * i);
        console.log(schannel + "-" + startchannel + "-" + i + "-" + Object.keys(obj.channels).length);
        if(startchannel + Object.keys(obj.channels).length > 255){
          universe++;
        }
        var device = {
          name: obj.name + i,
          universe: universe,
          startchannel: schannel,
          channels: {}
        }
      
        for(var k in obj.channels){
          device.channels[k] = obj.channels[k];
        }
        this.devices.push(device);
        
      }
      
    console.log(this.devices);
  }
  };
  
});

//DEVICE EXAMPLE
//{
//  name: string name,
//  universe: int universe,
//  startchannel: int startchannel,
//  channels: {
//    name: {v: int value, in: {m:0}, id: int chId, htp: boolean use htp by default}
//  }
//}