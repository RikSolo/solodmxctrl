var sDMXfile = angular.module('sDMXfile', ['sDMXpatch', 'sDMXscene', 'sDMXchase']);

var jsonfile = require('jsonfile');
const {dialog} = require('electron').remote;

sDMXfile.service('fileService', function(patchService, sceneService, chaseService){
  this.inputs = {};
  this.buttons = [];
  
  this.save = function(){
    var file = dialog.showSaveDialog({filters: [{name: 'sDMX file', extensions: ['sdmx']}], defaultPath:'./'});
    var object = {
      patch: patchService.devices,
      scenes: sceneService.scenes,
      chases: chaseService.chases,
      inputs: this.inputs,
      buttons: this.buttons
    };
    console.log(this.inputs);
    if(file){
      jsonfile.writeFile(file , object, function(err){
        if(err) throw err;
      });
    }
  }
  
  this.load = function(callBack){
    var file = dialog.showOpenDialog({properties: ['openFile'], filters: [{name: 'sDMX file', extensions: ['sdmx']}], defaultPath: './'});
    if(file){
    jsonfile.readFile(file[0], function(err, obj){
      if(err) throw err;
      patchService.devices = obj.patch;
      sceneService.scenes = obj.scenes;
      chaseService.chases = obj.chases;
      callBack(obj);
    })
    }
  }
})
