var sDMXuniverse = angular.module("sDMXuniverse", ['sDMXpatch']);

sDMXuniverse.service("universeService", function(patchService, $http){
  this.universes = [
    []
  ];
  
  this.host = 'localhost';
  this.port = 9090;
  
  this.calculateUniverses = function(){
    var devices = patchService.devices;
    for(var device in devices){
      if(this.universes.length + 1 <= devices[device].universe){
        this.universes[devices[device].universe] = [];
      }
      for(var channel in devices[device].channels){
        var curChannel = parseInt(devices[device].startchannel) + parseInt(devices[device].channels[channel].id) - 1;
        console.log(device);
        console.log(devices[device]);
        this.universes[devices[device].universe][curChannel] = parseInt(devices[device].channels[channel].v);
        
      }
    }

  }
  
  this.pushUniverses = function(){
    for(var i in this.universes){
      var universeString = this.universes[i].join(',');
      console.log(this.universes[i]);
      $http({
        method: 'POST',
        url: 'http://' + this.host + ':' + this.port + '/set_dmx',
        data: "u=0&d=" + universeString,
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      });
    }}
  
});