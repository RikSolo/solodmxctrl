var sDMXapp = angular.module('sDMXapp', ['sDMXuniverse', 'sDMXpatch', 'sDMXchannels', 'sDMXscene', 'sDMXfile', 'sDMXchase', 'sDMXinput', 'cfp.hotkeys']);


sDMXapp.controller("mainCtrl", function($scope, $http, hotkeys, patchService, universeService, channelService, sceneService, fileService, chaseService, inputService, $interval){
  
  $scope.sliders        = [];
  $scope.sceneSliders   = [];
  $scope.patch          = patchService;
  $scope.scenes         = sceneService;
  $scope.chases         = chaseService;
  $scope.channels       = channelService;
  $scope.files          = fileService;
  $scope.inputUniverse  = [];
  
  $scope.appMode = "main";
  $scope.editmode = false;
  
  $scope.buttons = [];
  $scope.inputs = {main:[], apc:[]};

  var inputInterval = 22;
  var inputUniverseNum = 99999;
  
  function init(){
    for(var i = 0; i < 24; i++){
      $scope.inputs.main.push({v:0, function: {mode:null, id:0, cap:null, name:null}, label: "", showEditor: false});
    }
    for(var i = 0; i < 9; i++){
      $scope.inputs.apc.push({v:0, function: {mode:null, id:0, cap:null, name:null}, label: "", showEditor: false});
    }
    for(var i = 0; i < 340; i++){
      $scope.buttons.push({running: false, flash: false, label: " ", function: {mode: null, id:0, cap: null, fade:0}, showEditor: false});
    }
  }
  init();
  
  $scope.toggleEditor = function(parent, index){
    var input = $scope.inputs[parent][index];
    if($scope.editMode){
      if(input.showEditor){
        input.showEditor = false;
      } else {
        input.showEditor = true;
      }
    } else {
      if(input.function.mode == "c"){
        chaseService.nextStep(input.function.id);
      }
    }
  }
  
  $scope.toggleButton = function(index){
    var button = $scope.buttons[index];
    if($scope.editMode){
      if(button.showEditor){
        button.showEditor = false;
      } else {
        button.showEditor = true;
      }
    } else {
      if(button.running == true){
        button.running = false;
      }else{
        button.running = true;
      }
      switch(button.function.mode){
        case "m":
          if(button.running){
            channelService.setHTP(parseInt(button.function.id), button.function.cap, "btn" + index, 255, button.function.fade);
            console.log(button);
          } else {
            channelService.setHTP(button.function.id, button.function.cap, "btn" + index, 0, button.function.fade);
          }
          universeService.pushUniverses();
          break;
        case "s":
          if(button.running){
            sceneService.startScene(button.function.id);
          }else{
            sceneService.stopScene(button.function.id);
          }
          break;
        case "tc":
          if(button.running){
            chaseService.startChase(button.function.id);
          }else{
            chaseService.stopChase(button.function.id);
          }
          break;
        case "nc":
          chaseService.nextStep(button.function.id);
          button.running = false;
          break;
      }
    }
  }
  
  $scope.releaseButton = function(index){
    var button = $scope.buttons[index];
    if(button.flash && button.function.mode != "nc"){
      $scope.toggleButton(index);
    }
  }
  
  
    $scope.$watch('inputs', function(newVal, oldVal){
      if($scope.appMode == "main"){
      for(var h in $scope.inputs){
      for(var i in $scope.inputs[h]){
        var input = $scope.inputs[h][i];
        if(newVal[h][i].v != oldVal[h][i].v){
        switch(input.function.mode){
          case "m":
            if(patchService.devices[input.function.id].channels[input.function.cap] != input.v && !isNaN(input.v)){
              channelService.setHTP(input.function.id, input.function.cap, "in", input.v);
              universeService.pushUniverses();
            }
            break;
          case "s":
            if(!isNaN(input.v)){
              sceneService.setScene(input.function.id, input.v);
            }
            break;
          case "c":
            if(!isNaN(input.v)){
              chaseService.setChase(input.function.id, input.v);
            }
          }
        }
      }
      }
      }
    }, true)
  
  function updateInputUniverse(){
    $scope.inputUniverse = inputService.getUniverse(inputUniverseNum);
    for(var i in $scope.inputs.main){
      $scope.inputs.main[i].v = $scope.inputUniverse[i];
    }
    
  } 
  $interval(updateInputUniverse, 22);
  
  $scope.setChannelManually = function(parent, index, key){
    channelService.setHTP(parent, key, "m", parseInt($scope.sliders[parent][index]));
    universeService.pushUniverses();
  }

  $scope.dumpScene = function(name){
    sceneService.dumpToScene(name);
    $scope.showDumpScene = false;
  }
  
  $scope.save = function(){
    fileService.inputs = $scope.inputs;
    fileService.buttons = $scope.buttons;
    fileService.save();
  }
  
  $scope.load = function(){
    fileService.load(function(obj){
      $scope.inputs = obj.inputs;
      $scope.buttons = obj.buttons;
    })
  }
  
  hotkeys.add({
    combo: 'ctrl+s',
    description: 'save',
    callback: function(){
      $scope.save();
    }
  });
  hotkeys.add({
    combo: 'ctrl+o',
    description: 'load',
    callback: function(){
      $scope.load();
    }
  });
  hotkeys.add({
    combo: 'ctrl+d',
    description: 'dump',
    callback: function(){
      $scope.showDumpScene = true;
    }
  })
});

